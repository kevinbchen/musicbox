/**
 * @file common.h
 * @brief Common definitions and macros.
 *
 * @author Kevin Chen <<kchen2@caltech.edu>>
 */
#ifndef _COMMON_H_
#define _COMMON_H_

// Represent bool as uint8_t with 1 = true, 0 = false
typedef uint8_t bool;
#define true    1
#define false   0

// Defines for nested macros (used for pin macros below)
#define P(a, b) a ## b
#define P2(a, b) P(a, b)

// Set a pin as input or output
#define set_input(bit)              P2(DDR, bit ## _PORT) &= ~(1 << bit)
#define set_output(bit)             P2(DDR, bit ## _PORT) |= (1 << bit) 

// Enable or disable the pullup of a pin (input)
#define pullup_on(bit)              P2(PORT, bit ## _PORT) |= (1 << bit)
#define pullup_off(bit)             P2(PORT, bit ## _PORT) &= ~(1 << bit)

// Set the state of an output pin
#define set_high(bit)               P2(PORT, bit ## _PORT) |= (1 << bit)
#define set_low(bit)                P2(PORT, bit ## _PORT) &= ~(1 << bit)
#define toggle(bit)                 P2(PORT, bit ## _PORT) ^= (1 << bit)

// Check the state of an input pin
#define is_high(bit)                (P2(PIN, bit ## _PORT) & (1 << bit)) 
#define is_low(bit)                 (! (P2(PIN, bit ## _PORT) & (1 << bit))) 

#endif
