import sys

name = raw_input('name: ')
shift = int(raw_input('pitch shift (12 = octave): '))
print 'paste from MidiToC program:'


data = ''
for line in sys.stdin:
	if line.find('s_melody_events') != -1:  break

max_delay = 0
for line in sys.stdin:
	if line.find('};') != -1:  break

	s = line.split()
	note = int(s[1][:-1])
	if note != 0:
		note = max(0, note + shift)
	delay = int(s[2])
	max_delay = max(max_delay, delay)
	data += '    {%d, %d, %d},\n' % (note, delay % 256, delay / 256)

print max_delay


f = open('../songs/%s.h' % (name), 'w')
f.write('const uint8_t %s[][3] PROGMEM = {\n' % (name))
f.write(data)
f.write('};\n')
f.close();