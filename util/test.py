import wave
import numpy as np

wr = wave.open('a5000.wav', 'r')
sz = 100000 # Read and process 1 second at a time.
da = np.fromstring(wr.readframes(sz), dtype=np.int16)
wr.close()
left, right = da[0::2], da[1::2]
print len(left)

lo = sum(sorted(left)[:100]) / 100;
hi = sum(sorted(left)[-100:]) / 100;
scale = 255 / float(hi - lo);

for i in xrange(len(left)):
	left[i] = int((left[i] - lo) * scale)
	if left[i] > 255:
		left[i] = 255
	elif left[i] < 0:
		left[i] = 0


for i in xrange(len(left)):
	if i % 100 == 0:
		print ''	
	print str(left[i]) + ',',
