/**
 * @file musicbox.c
 * @brief Music Box main function.
 *
 * @author Kevin Chen <<kevinchen1992@gmail.com>>
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <util/delay.h> 
#include "common.h"
#include "tone.h"

#include "songs/spiritedaway_itsumo.h"
#include "songs/totoro.h"
#include "songs/laputa.h"
#include "songs/spiritedaway_inochi.h"
#include "songs/river.h"
#include "songs/simpleandclean.h"
#include "songs/moonsong.h"


/*#include "songs/unicorn.h"
#include "songs/kimiboku.h"
#include "songs/meteor.h"
#include "songs/justcommunication.h"
#include "songs/reason.h"*/


#define LED2        PB2
#define LED2_PORT   B
#define LED1        PB3
#define LED1_PORT   B
#define SPKR1       PD6
#define SPKR1_PORT  D
#define SPKR2       PD5
#define SPKR2_PORT  D


#define NUM_SONGS       sizeof(songs) / sizeof(songs[0])
#define NUM_CHANNELS    4
#define NUM_KEYS        14
#define DEBOUNCE_COUNT  3


typedef struct {
    uint16_t count;
    uint16_t add;
    uint16_t env_count;
} Channel;

typedef struct {
    volatile uint8_t *port;
    volatile uint8_t *pin;
    uint8_t bit;
    uint16_t note;
    int8_t count;
} Key;


Channel channels[NUM_CHANNELS];

const uint8_t (*songs[])[][3] = {
    &spiritedaway_itsumo,    
    &totoro,
    &laputa, // -6 pitch shift
    &spiritedaway_inochi,
    &river,
    &simpleandclean,
    &moonsong,

    /*&unicorn,
    &kimiboku,
    &meteor,
    &justcommunication,
    &reason,*/

};


Key keys[NUM_KEYS] = {
    {&PORTC, &PINC, PC5, 3, 0},   // C
    {&PORTC, &PINC, PC4, 4, 0},
    {&PORTD, &PIND, PD0, 5, 0},   // D
    {&PORTD, &PIND, PD1, 6, 0},
    {&PORTC, &PINC, PC3, 7, 0},   // E
    {&PORTD, &PIND, PD2, 8, 0},   // F
    {&PORTC, &PINC, PC2, 9, 0},
    {&PORTD, &PIND, PD3, 10, 0},  // G
    {&PORTC, &PINC, PC1, 11, 0},   
    {&PORTD, &PIND, PD4, 12, 0},  // A
    {&PORTC, &PINC, PC0, 13, 0},   
    //{&PORTD, &PIND, PD7, 14, 0},  // B
    {&PORTB, &PINB, PB4, 14, 0},  // B    
    {&PORTB, &PINB, PB1, 14, 0},  // B
    {&PORTB, &PINB, PB0, 14, 0},  // B
};



bool autoMode = true;

int16_t note_index = 0;
uint16_t delay = 1;
int16_t song_index = 0;

uint8_t key_index = 0;
uint8_t channel_index = 0;
uint8_t octave = 5;

uint8_t update_count = 0;

uint8_t led_delay = 0;
uint8_t led_pwm_count = 0;
uint8_t led_intensity = 0;

int main(void) {
    // power stuff
    power_adc_disable();
    power_spi_disable();
    power_twi_disable();
    power_usart0_disable();
    power_timer2_disable();

    // set outputs
    set_output(LED1);
    set_output(LED2);
    set_output(SPKR1);
    set_output(SPKR2);
    set_high(LED1); // power indicator

    // set inputs
    for (int i = 0; i < NUM_KEYS; i++) {
        *keys[i].port |= _BV(keys[i].bit);
        keys[i].note = notes_add[keys[i].note + 7 * 12];
    }

    // initialize channel variables
    for (int i = 0; i < NUM_CHANNELS; i++) {
        channels[i].count = 0;
        channels[i].add = 0;
        channels[i].env_count = 0;
    }

    note_index = -1;
    delay = 1;

    // Timer 0 for PWM of speaker pins (to simulate amplitudes)
    // Fast PWM, 16 MHz tick
    TCCR0A |= _BV(COM0A1) | _BV(COM0B1);
    TCCR0A |= _BV(WGM00) | _BV(WGM01) | _BV(WGM02);
    TCCR0B |= _BV(CS00);

    // Timer 1 for amplitude (waveform and envelope) updates
    // CTC, 16 MHz
    // 18 KHz interrupt (16 Mhz / 889)
    TCCR1B |= _BV(WGM12);
    TCCR1B |= _BV(CS10);
    TIMSK1 |= _BV(OCIE1A);
    OCR1A = 667;

    // Timer 2 for note updates / key debouncing
    // CTC, 16 Mhz / 1024 = 15.625 Khz
    // ~260 Hz interrupt (15.625 KHz / 60)
    /*TCCR2A |= _BV(WGM21);
    TCCR2B |= _BV(CS22) | _BV(CS21) | _BV(CS20);
    TIMSK2 |= _BV(OCIE2A);
    OCR2A = 60;*/
    
    // turn on interrupts
    sei();

    while (true);

    return 0;
}

ISR(TIMER1_COMPA_vect) {
    uint16_t out = 0;
    if (led_pwm_count == 0) led_intensity = 0;
    for (int i = 0; i < NUM_CHANNELS; i++) {
        channels[i].count += channels[i].add;
        uint8_t env_index = channels[i].env_count >> 8;

        //if (i == 0 && env_index == 20) set_low(LED2);

        if (env_index < 128) {
            channels[i].env_count++;        
            uint16_t wave_val = waveform[channels[i].count >> 10];
            uint8_t env_val = envelope[env_index];
            out += ((wave_val * env_val) >> 8);// >> (i + 1);
            if (led_pwm_count == 0) led_intensity += led_envelope[env_index];
        } else {
            out += 0;
        }


        if (i == NUM_CHANNELS / 2 - 1) {
            OCR0A = out >> 1;
            out = 0;
        }
    }
    OCR0B = out >> 1;

    // software pwm the led
    if (led_pwm_count == 0 && led_intensity != 0) {
        set_high(LED2);
    } else if (led_pwm_count == led_intensity) {
        set_low(LED2);
    }
    led_pwm_count++;


    // note updating and key handler
    update_count++;
    if (update_count == 92) {  // 138 at 36khz
        update_count = 0;

        if (autoMode) {
            delay--;
            if (delay == 0) {
                note_index++;
                uint8_t note = pgm_read_byte(&(*songs[song_index])[note_index][0]);
                uint16_t data = pgm_read_word(&(*songs[song_index])[note_index][1]);
                uint8_t c_index = data & 0x3;
                delay = data >> 3;
                if (note != 0) {
                    channels[c_index].add = notes_add[note + 12 - 21];
                    channels[c_index].env_count = 0;

                    //if (c_index == 0) set_high(LED2);
                    //set_high(LED2);
                    led_delay = delay / 3;
                    //led_intensity = 25;
                } else {
                    if (delay == 0) {
                        // end of song
                        delay = 500;
                        note_index = -1;
                        song_index++;
                        if (song_index >= NUM_SONGS) song_index = 0;
                    }
                }
                channels[c_index].count = 0;            
            } else if (delay == led_delay) {
                //led_intensity = 0;
                //set_low(LED2);
            }
        }

        int i = key_index;
        if (*keys[i].pin & _BV(keys[i].bit)) {
            keys[i].count--;
            if (keys[i].count < 0) keys[i].count = 0;
        } else {
            if (keys[i].count == 0) {
                keys[i].count = DEBOUNCE_COUNT;

                if (autoMode) {
                    if (i == 12) {
                        delay = 200;
                        note_index = -1;
                        song_index--;
                        if (song_index < 0) song_index = NUM_SONGS - 1;                                                         
                    } else if (i == 13) {
                        delay = 200;
                        note_index = -1;
                        song_index++;
                        if (song_index >= NUM_SONGS) song_index = 0;                                     
                    } else {
                        autoMode = !autoMode;
                    }
                } else {
                    if (i == 12) {
                        if (octave > 0) octave--;
                    } else if (i == 13) {
                        if (octave < 7) octave++;
                    } else {
                        uint16_t note = keys[i].note >> (7 - octave);
                        uint8_t c_index = channel_index;
                        channels[c_index].add = note;
                        channels[c_index].env_count = 0;
                        channels[c_index].count = 0;

                        channel_index = (channel_index + 1) % NUM_CHANNELS;
                    }
                }
            }
        }
        key_index++;
        if (key_index >= NUM_KEYS) key_index = 0;
    }
}

// NOTE: this could be done manually in the Timer 1 interrupt (with counter)
/*ISR(TIMER2_COMPA_vect) {
}*/